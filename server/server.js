import http from 'http';
import fs from 'fs';
import url from 'url';
import path from 'path';

//Функция, которая будет использоваться в качестве коллбэка для метода http.createServer
//Функция обрабатывает входящие запросы и отправляет ответ
function requestHandler(request, response){
  // Определим путь к запрашиваемому ресурсу и присвоим переменной
  let requestedResource = path.join(
    __dirname, //место расположения данного файла
    '../public', // выходим из этой директории и заходим в директорию public,
    url.parse(request.url).pathname // путь к ресурсу, запрошенного клиентом
  );

  //используем метод exists модуля fs для проверки существования пути из requestResource
  fs.exists(requestedResource, function(exists) {
    // проверка отсутствия файла и вывод в этом случае статуса 404 (Файл не найден)
    if(!exists) {
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    // Проверка наличия запрашиваемого ресурса в директории и, при наличии запрашиваемого ресурса, просто зададим страницу index.html как запрошенный ресурс
    if (fs.statSync(requestedResource).isDirectory()) {
      requestedResource += '/index.html';
    }

    // И наконец, прочитаем запрошенный файл (сделаем это асинхронно) и отправим его содержание клиенту
    fs.readFile(
      requestedResource, //путь к запрашиваемому ресурсу
      "binary", // прочитать запрошенный ресурс как двоичный файл
      function(err, file) { // коллбэк для обработки чтения конца файла

        // В случае возникновения ошибки в процессе чтения файла отправить сообщение об ошибке со статус кодом 500 (Внутренняя ошибка сервера)
        if (err) {
          response.writeHead(500, {"Content-Type": "text/plain"});
          response.write(err + "\n");
          response.end();
          return;
        }

        // Объект-помощник для преобразования типов (расширений) содержимого ответа на запрос к mime-типам 
        const contentTypesByExtension = {
          '.html': "text/html",
          '.css': "text/css",
          '.js': "text/javascript"
        };

        // Объект-помощник для заголовков
        const headers = {};
        // Получаем тип содержимого используя расширение запрошенного файла
        const contentType = contentTypesByExtension[
          path.extname(requestedResource)
        ];

        //Если запрошеный ресурс преобразуется к одному из наших типов содержимого, тогда зададим поле Content-Type к нашим заголовкам для ответа на запрос
        if (contentType) {
          headers["Content-Type"] = contentType;
        }

        response.writeHead(200, headers); // записываем заголовки при наличии
        response.write(file, "binary"); // записываем содержание читаемого файла в двоичном формате
        response.end(); // отправляем ответ и закрываем запрос
      }
    );
  });
}

// Создадем экземпляр httpServer и отправляем в наш коллбэк-обработчик запроса requestHandler
const server = http.createServer(requestHandler);
// Объявим номер порта
const portNumber = 3030;
// Поставим сервер слушать указанный порт
server.listen(portNumber, function () {
  // Залогируем в консоль, чтобы знать что наш сервер запущен
  console.log(`Server listening on port ${portNumber}`);
});