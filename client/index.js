import React from 'react';
import ReactDOM from 'react-dom';
import Frame from './components/frame'
import './styles/main.css';

// Использую метод render мы подключим этот узел в наш DOM (html файл) к элементу с id='app'
ReactDOM.render(
  <Frame />,
  document.getElementById('app')
);