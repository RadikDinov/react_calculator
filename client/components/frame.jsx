import React from 'react';
import Screen from './screen';
import Button from './button';

// Создаем класс наследующий react-компонент
class Frame extends React.Component {
  constructor() {
    super();
    // Зададим состояние по умолчанию
    this.state = {
      question: '',
      answer: ''
    }
    // Привяжем handleClick метод (т.е. устанавливаем 'this' явно к этому компоненту). Это делается потому что 'this' ссылается к источнику события нажатия
    this.handleClick = this.handleClick.bind(this);
  }

  //Функция render создает компонент для отрисовки в DOM. Этот метод должен вернуть один родительский элемент. Компонент обернут в скобки для того, чтобы создать единое выражение
  render() {
    return (
      <div className="frame">
        <div className="calculator-title">
          R_N Calculator
        </div>
        <Screen question={this.state.question} answer={this.state.answer}/>
        <div className="button-row">
          <Button label={'1'} handleClick={this.handleClick} type='input' />
          <Button label={'2'} handleClick={this.handleClick} type='input' />
          <Button label={'3'} handleClick={this.handleClick} type='input' />
          <Button label={'4'} handleClick={this.handleClick} type='input' />
          <Button label={'+'} handleClick={this.handleClick} type='action' />
          <Button label={'-'} handleClick={this.handleClick} type='action' />
        </div>
        <div className="button-row">
          <Button label={'5'} handleClick={this.handleClick} type='input' />
          <Button label={'6'} handleClick={this.handleClick} type='input' />
          <Button label={'7'} handleClick={this.handleClick} type='input' />
          <Button label={'8'} handleClick={this.handleClick} type='input' />
          <Button label={'*'} handleClick={this.handleClick} type='action' />
          <Button label={'/'} handleClick={this.handleClick} type='action' />
        </div>
        <div className="button-row">
          <Button label={'9'} handleClick={this.handleClick} type='input' />
          <Button label={'.'} handleClick={this.handleClick} type='input' />
          <Button label={'0'} handleClick={this.handleClick} type='input' />
          <Button label={'Cls'} handleClick={this.handleClick} type='action' />
          <Button label={'='} handleClick={this.handleClick} type='action' />
        </div>
      </div>
    );
  }

  // Метод для обработки всех событий нажатия на кнопки
  handleClick(event){
    const value = event.target.value
    switch (value) {
      case '=': {// Если нажата кнопка со знаком равенства, использовать eval модуль для определения запроса (question)
        // конвертируем ответ (числовой) в строку
        const answer = eval(this.state.question).toString();
        //Обновляем ответ в в нашем состоянии
        this.setState({ answer });
        break;
      }
      case 'Cls': {
        // Очистка quection и answer
        this.setState({ question: '', answer: '' });
        break;
      }
      default: {
        // Для всех остальных команд, обновить ответ в состоянии компонента
        this.setState({ question: this.state.question += value });
        break;
      }
    }
  }
}

//Экспортируем наш Frame-компонент для использования в файле client/index.js
export default Frame;