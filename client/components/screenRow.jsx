import React from 'react';

// ScreenRow компонент написан как функциональный компонент, получающий и отображающий (в поле ввода) значения параметров (props) родительского компонента
const ScreenRow = (props) => {
  return (
    <div className="screen-row">
      <input type="text" readOnly value={props.value}/>
    </div>
  )
}

// Описываем параметры (props), которые родительский элемент должен передать в этот компонент
ScreenRow.propTypes = {
  value: React.PropTypes.string.isRequired
}

export default ScreenRow;