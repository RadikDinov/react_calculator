import React from 'react';
import ScreenRow from './screenRow';

// Создадим компонент функциональным. Компонент будет отображать два ScreenRow компонента, первый для ввода запроса, второй для вывода ответа. Значение будет передано от родительского компонента в качестве параметра (prop)
const Screen =(props) => {
  return (
    <div className="screen">
      <ScreenRow value={props.question} />
      <ScreenRow value={props.answer} />
    </div>
  );
}

// Определим параметры, ожидаемые от родительского компонента
Screen.propTypes = {
  question: React.PropTypes.string.isRequired,
  answer: React.PropTypes.string.isRequired
}

export default Screen;